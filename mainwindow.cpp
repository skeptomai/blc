#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newstudentform.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Database Connection
    QSqlDatabase BLCDB = QSqlDatabase::addDatabase("QSQLITE");
    QString appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    if (!QDir(appDataPath).exists()) QDir(appDataPath).mkdir(".");
    BLCDB.setDatabaseName(appDataPath + "/BLCDB.db");
    if(!BLCDB.open()) {
        QMessageBox::information(this,"ERROR", "DB Not Connected");
    }

    if (!BLCDB.tables().contains("STUDENT")){
    //Init Database
        QSqlQuery createTableQuery(BLCDB);
        qDebug() << "creating new db";
        createTableQuery.exec("CREATE TABLE STUDENT (SID INTEGER PRIMARY KEY AUTOINCREMENT, Name STRING NOT NULL DEFAULT Mario, Surname   STRING NOT NULL DEFAULT Rossi, Birthday  DATE   NOT NULL DEFAULT (1 / 1 / 1900), Address   STRING, Telephone STRING, CF STRING);");



    }

    // Init Toolbar
    QToolBar *tb = this->addToolBar("");
    QPushButton *addButton = new QPushButton("Add Student",tb);
    QLineEdit *searchBar = new QLineEdit(tb);
    QPushButton *searchButton = new QPushButton("Search",tb);
    tb->addWidget(addButton);
    tb->addWidget(searchBar);
    tb->addWidget(searchButton);

    QObject::connect(searchButton,SIGNAL(clicked()),this,SLOT(execSearch()));
    QObject::connect(addButton,SIGNAL(clicked()),this,SLOT(openAddForm()));



    // Init Table Widget

    ui->tableWidget->setRowCount(10);
    ui->tableWidget->setColumnCount(5);
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    //Table Header
    ui->tableWidget->setHorizontalHeaderItem(0,new QTableWidgetItem("SID"));
    ui->tableWidget->setHorizontalHeaderItem(1,new QTableWidgetItem("Name"));
    ui->tableWidget->setHorizontalHeaderItem(2,new QTableWidgetItem("Surname"));
    ui->tableWidget->setHorizontalHeaderItem(3,new QTableWidgetItem("Birthday"));
    ui->tableWidget->setHorizontalHeaderItem(4,new QTableWidgetItem("Address"));

    MainWindow::LoadData();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::LoadData()
{

    ui->tableWidget->clearContents();
    QString *filter = new QString(this->findChild<QToolBar *>("")->findChild<QLineEdit *>()->text());

    QString *queryString1 = new QString("SELECT * FROM STUDENT WHERE Name LIKE '%");
    QString *queryString2 = new QString("%' OR Surname LIKE '%");
    QString *queryString3 = new QString("%'");
    QString result = *queryString1 + *filter + *queryString2 + *filter + *queryString3;

    // Retrieve Data from DB
    QSqlQuery query(QSqlDatabase::database());
    query.exec(result);
    int row = 0;
    while (query.next()) {
        ui->tableWidget->setItem(row,0, new QTableWidgetItem(query.value(0).toString()));
        ui->tableWidget->setItem(row,1, new QTableWidgetItem(query.value(1).toString()));
        ui->tableWidget->setItem(row,2, new QTableWidgetItem(query.value(2).toString()));
        ui->tableWidget->setItem(row,3, new QTableWidgetItem(query.value(3).toString()));
        ui->tableWidget->setItem(row,4, new QTableWidgetItem(query.value(4).toString()));
        row++;
    }



}

void MainWindow::execSearch()
{
    MainWindow::LoadData();
}

void MainWindow::openAddForm()
{
    NewStudentForm *nsf = new NewStudentForm(this);
    nsf->show();
}
