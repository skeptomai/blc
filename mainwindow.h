#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QMessageBox>
#include <QToolBar>
#include <QLineEdit>
#include <QPushButton>
#include <QStandardPaths>
#include <QStringBuilder>
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void LoadData();

private:
    Ui::MainWindow *ui;

private slots:
    void execSearch();
    void openAddForm();
};


#endif // MAINWINDOW_H
