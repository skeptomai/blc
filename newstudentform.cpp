#include "newstudentform.h"
#include "ui_newstudentform.h"

NewStudentForm::NewStudentForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewStudentForm)
{
    ui->setupUi(this);
    QPushButton *saveButton = ui->buttonBox->button(QDialogButtonBox::Save);

    QObject::connect(saveButton,SIGNAL(clicked()),this,SLOT(submitInsert()));
}

NewStudentForm::~NewStudentForm()
{
    delete ui;
}

void NewStudentForm::submitInsert()
{
       QSqlQuery insertQuery(QSqlDatabase::database());
       QString *insertQueryString = new QString("INSERT INTO STUDENT(Name,Surname,Birthday,Address,Telephone,CF) VALUES('"
       + ui->studentNameLineEdit->text() + "', '"
       + ui->surnameLineEdit->text() + "', "
       + ui->birthdayDateEdit->text() + ", '"
       + ui->addressLineEdit->text() + "', '"
       + ui->telephoneLineEdit->text() + "', '"
       + ui->cFLineEdit->text() + "');");

       qDebug() << *insertQueryString;

       insertQuery.exec(*insertQueryString);


}
