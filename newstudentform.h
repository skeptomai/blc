#ifndef NEWSTUDENTFORM_H
#define NEWSTUDENTFORM_H

#include <QDialog>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDialogButtonBox>
#include <QPushButton>

namespace Ui {
class NewStudentForm;
}

class NewStudentForm : public QDialog
{
    Q_OBJECT

public:
    explicit NewStudentForm(QWidget *parent = nullptr);
    ~NewStudentForm();

private:
    Ui::NewStudentForm *ui;

private slots:
    void submitInsert();

};

#endif // NEWSTUDENTFORM_H
